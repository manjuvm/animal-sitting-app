    const animal = document.getElementById("animal")
    const breed = document.getElementById("breed")
    const date = document.getElementById("date")
    const actions = document.getElementById("actionBtn")

    const animalData = {Dog:['Labrador', 'German Shepherd', 'Golden Retriever', 'Bulldog', 'Beagle', 'Boxer', 'Dachshund', 'Great Dane'],
    Cat:['Bengal', 'Bombay', 'Persian', 'Russian', 'Savannah', 'Siamese', 'Sphynx'],
    Bird:['Woodpecker', 'Pigeon', 'Peacock', 'Rooster', 'Vulture', 'Swallow', 'Seagull', 'Quail', 'Duck', 'Pelican', 'Magpie', 'Parrot', 'Turkey', 'Crane', 'Kingfisher', 'Hummingbird', 'Sparrow', 'Cormorant', 'Ostrich', 'Crow'], 
    Rabbit:['Tan', 'Jersey Wooly', 'English Lop', 'Beveren', 'Chinchilla', 'Himalayan', 'Belgian', 'Sable', 'Silver Fox']}

    function createNew(){
        animal.style.display ="block"
        actions.style.display ="block"
    }

    function selectAnimal(){
        breed.style.display ="block"
        const selectedAnimal = $('#animalInput').val();
        const relatedBreed = animalData[selectedAnimal]

        var breedOptions = $('#breedInput')
        breedOptions.empty()
        breedOptions.append(`<option></option>`)
        let options = relatedBreed.map((item)=> {
            breedOptions.append(`<option>${item}</option>`)
        })
    }

    function selectBreed(){
        date.style.display ="block"
    }

    function selectDate(){
        actions.style.display ="block"
    }

    function cancelAll(){
        animal.style.display ="none"
        breed.style.display ="none"
        date.style.display ="none"
        actions.style.display ="none"
        $('#animalInput').val("");
        $('#breedInput').val("");
        $('#dateInput').val("");
    }

    

    function validateInput(selectedAnimal, selectedBreed, sittingDate){
        let isValid = true
        if(!selectedAnimal){
            isValid = false
            alert("Please select animal")
            return false;
        }
        if(!selectedBreed){
            isValid = false
            alert("Please select breed")
            return false;
        }

        if(!sittingDate){
            isValid = false
            alert("Please select sitting date")
            return false;
        }
        return isValid
    }

    function saveSitting(){
        var selectedAnimal = $('#animalInput').val();
        var selectedBreed = $('#breedInput').val();
        var sittingDate = $('#dateInput').val();

        if(validateInput(selectedAnimal, selectedBreed, sittingDate)){
        $('#animalSittingTable tr:last').after(`<tr><td>${selectedAnimal}</td>
            <td>${selectedBreed}</td>
            <td>${sittingDate}</td>
            </tr>`);
        cancelAll()
    }
    }
