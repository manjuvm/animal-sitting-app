from django.shortcuts import render
from django.http import HttpResponse

def animal_sitting(request):
    return render(request, 'sitting/animal_sitting_list.html')
